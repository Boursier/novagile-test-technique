<strong>Le but :</strong>

Réaliser en vuejs un tableau pour afficher les données présentes dans le fichier json,  sans réinventer la roue.
A toi d’afficher les données comme bon te semble mais de manière la plus logique possible. (ex : le gender n’est pas une donnée prioritaire)

On doit pouvoir :
<ul
    <li>Chercher dans le tableau</li>
    <li>Appliquer des filtres et order sur les colonnes</li>
    <li>Réordonner les colonnes</li>
    <li>à toi d’ajouter d’autres fonctionnalités qui te semblent utiles</li>
</ul>

Les dates sont au format iso 8601 (utc)