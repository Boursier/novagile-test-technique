// Composant modal
Vue.component('modal', {
    template: '#modal-template',
})

// Application
var nvg = new Vue({
    el: '#novagile-test',
    data: {
        searchQuery: '',
        columns: {
            attachments: 'PJ',
            email: 'De',
            status: 'Status',
            interaction_creation_date: 'Création',
            due_date: 'Echéance',
            assignedTO: 'Assigné',
            last_comment: 'Dernier commentaire'
        },
        datas: [], 
        showModal: false,
        sortKey: '',
        sortOrders: [],
        nbPages: 1,
        currentPage: 0
    },
    mounted: function () {
        var self = this
        //On initialise à 1 tous les filtres pour chaques colonnes
        for (var column in self.columns) {
            self.$set(self.sortOrders, column, 1);
        }
        //On récupère le fichier Json via une requête Ajax
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'mock_data.json', true);
        xhr.responseType = 'json';
        xhr.onload = function () {
            var status = xhr.status;
            if (status === 200) {
                var datasResponse = xhr.response;
                //On applatit le sous tableau customer
                for (var data in datasResponse) {
                    datasResponse[data].email = datasResponse[data].customer.email;
                    datasResponse[data].first_name = datasResponse[data].customer.first_name;
                    datasResponse[data].last_name = datasResponse[data].customer.last_name;
                    delete datasResponse[data].customer;
                }
                self.datas = datasResponse; //Affectation du resultat à la propriété datas
                self.nbPages = datasResponse.length / 100; // Nombre de pages
            } else {
                console.log(xhr.response)
            }
        };
        xhr.send();
    },
    methods: {
        getPage: function (value) {
            this.currentPage = value - 1;

        },
        getCurrentPage: function(value){
            if(value - 1 == this.currentPage){
                 return 'active';
            } 
        },
        getNextPage: function(){
            var nextPage = this.currentPage + 1;
            if(nextPage < this.nbPages){
                this.currentPage = nextPage;
            }
        },
        getPreviousPage: function(){
            var previousPage = this.currentPage - 1;
            if(previousPage >= 0){
                this.currentPage = previousPage;
            }
        },
        sortBy: function (key) {
            this.sortKey = key
            this.sortOrders[key] = this.sortOrders[key] * -1
        },
        getStatusClass: function (status) {
            switch (status) {
                case 'waiting':
                    return 'badge badge-warning';
                case 'reserved':
                    return 'badge badge-dark';
                case 'inProgress':
                    return 'badge badge-primary';
                case 'toTreat':
                    return 'badge badge-info';
                case 'finished':
                    return 'badge badge-success';
            }
        },
        deletePersonn: function (index) {
            console.log(index);
            this.datas.splice(index, 1);
            return this.datas;
        },
        //Affiche la modale et lui affecte son contenu
        displayModal: function (user_id) {
            var self = this;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'mock_data.json', true);
            xhr.responseType = 'json';
            xhr.onload = function () {
                var status = xhr.status;
                if (status === 200) {
                    var datasResponse = xhr.response;
                    var personne = datasResponse.filter(function (item) {
                        return item.id === user_id;
                    });
                    self.title = personne[0].customer.first_name + " " + personne[0].customer.last_name;
                    self.description =
                            "<strong>Email : </strong>" + personne[0].customer.email + "<br>"
                            + "<strong>Genre : </strong>" + personne[0].customer.gender + "<br>"
                            + "<strong>Code pays : </strong>" + personne[0].customer.country_code + "<br>"
                            + "<strong>Téléphone : </strong>" + personne[0].customer.phone + "<br>"
                            + "<strong>Date de naissance : </strong>" + self.$options.filters.formatDate(personne[0].customer.birthday) + "<br>"
                            + "<strong>Channel de contact : </strong>" + personne[0].contact_channel + "<br>"
                            + "<strong>Statut : </strong>" + personne[0].status + "<br>"
                            + "<strong>Création : </strong>" + self.$options.filters.formatDate(personne[0].interaction_creation_date) + "<br>"
                            + "<strong>Echéance : </strong>" + self.$options.filters.formatDate(personne[0].due_date) + "<br>"
                            + "<strong>Assigné : </strong>" + personne[0].assignedTO + "<br>"
                            + "<strong>Dernier commentaire : </strong>" + personne[0].last_comment + "<br>";
                    self.showModal = true;
                } else {
                    console.log(xhr.response)
                }
            };
            xhr.send();
        }
    },
    filters: {
        truncate: function (str) {
            return str.substring(0, 100) + " ...";
        },
        formatDate: function (str) {
            return str.replace('Z', '').replace('T', ' ');
        }
    },
    computed: {
        filteredData: function () {
            var sortKey = this.sortKey
            var filterKey = this.searchQuery && this.searchQuery.toLowerCase()
            var order = this.sortOrders[sortKey] || 1
            var datas = this.datas;
            if (filterKey) {
                datas = datas.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                        return String(row[key]).toLowerCase().indexOf(filterKey) > -1
                    })
                })
            }
            if (sortKey) {
                datas = datas.slice().sort(function (a, b) {
                    a = a[sortKey]
                    b = b[sortKey]
                    return (a === b ? 0 : a > b ? 1 : -1) * order
                })
            }

            // Modification de la pagination en fonction du filtre en cours
            var nbPages = Math.round(datas.length / 100);
            if (nbPages <= 1) {
                nbPages = 1;
            }
            this.nbPages = nbPages;
            if(nbPages < this.currentPage){
                this.currentPage = 0 ;
            }
            var start = this.currentPage * 100;
            var end = start + 100;

            // Retourne une portion du tableau correspondante à la page en cours
            return datas.slice(start, end)
        }
    }

})